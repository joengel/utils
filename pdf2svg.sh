#!/bin/sh
# run this in a directory containing the pdfs to be converted.
# mutool convert
# imagemagick convert and inkspace should work as well

for f in *.pdf
do
	mutool convert -F svg -o /tmp/${f%%.pdf}.svg $f
	mv "/tmp/${f%%.pdf}1.svg" \
		"`echo ./${f%%.pdf}1.svg | sed 's/1\.svg/\.svg/'`"
done

