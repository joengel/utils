#!/bin/sh

FILE=$1

HARDNESS=$(head -n1 $FILE | tr "," "\n" |grep -n "HARDNESS"| cut -d':' -f1)
MODULUS=$(head -n1 $FILE | tr "," "\n" |grep -n "MODULUS"| cut -d':' -f1)
XPOS=$(head -n1 $FILE | tr "," "\n" |grep -n "X Position"| cut -d':' -f1)
YPOS=$(head -n1 $FILE | tr "," "\n" |grep -n "Y Position"| cut -d':' -f1)

awk -F',' \
	-v hardness=${HARDNESS} \
	-v modulus=${MODULUS} \
	-v xpos=${XPOS} \
	-v ypos=${YPOS} \
	'NR>2{print $hardness"\t" $modulus"\t" $xpos"\t" $ypos"\t"}' $FILE
