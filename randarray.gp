#set terminal epslatex
set output "randsurf.tex"
set palette cubehelix
set pm3d
set hidden3d
set dgrid3d 50,50 qnorm 2
splot 'rs.dat'
