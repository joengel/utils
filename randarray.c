#include <stdio.h>
#include <stdlib.h>

int main ( int argc, char *argv[])
{	
	int i, j;

	if ( (--argc) == 2) {
		for (i = 0; i < atoi(argv[2]); i++) {
			for (j = 0; j < atoi(argv[1]); j++) {
				printf( "%d\t", arc4random());
			}
			printf( "\n");
		}
		return 0;
	}
	if ( (--argc) == 1) {
		for (i = 0; i < atoi(argv[1]); i++) {
			printf( "%d\t%d\n", i, arc4random());
		}
		return 0;
	}
	return 1;
}
